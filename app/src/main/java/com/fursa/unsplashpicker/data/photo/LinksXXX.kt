package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class LinksXXX(
    val html: String,
    val photos: String,
    val related: String,
    val self: String
)