package com.fursa.unsplashpicker.data.collections


import com.google.gson.annotations.SerializedName

data class ProfileImageX(
    val large: String,
    val medium: String,
    val small: String
)