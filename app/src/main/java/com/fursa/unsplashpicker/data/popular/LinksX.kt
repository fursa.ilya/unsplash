package com.fursa.unsplashpicker.data.popular


import com.google.gson.annotations.SerializedName

data class LinksX(
    val followers: String,
    val following: String,
    val html: String,
    val likes: String,
    val photos: String,
    val portfolio: String,
    val self: String
)