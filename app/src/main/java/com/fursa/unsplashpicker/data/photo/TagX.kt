package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class TagX(
    val source: SourceX,
    val title: String,
    val type: String
)