package com.fursa.unsplashpicker.data

import com.fursa.unsplashpicker.data.collections.GalleryResp
import com.fursa.unsplashpicker.data.photo.PhotoItemResp
import com.fursa.unsplashpicker.data.photos.PhotoResp
import com.fursa.unsplashpicker.data.popular.DailyResp
import com.fursa.unsplashpicker.data.search.SearchResp
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkApiService {

    companion object {
       operator fun invoke(): NetworkApiService {
           val requestInterceptor = Interceptor { chain ->  

             val url = chain.request()
                 .url
                 .newBuilder()
                 .addQueryParameter("client_id", "fb7c3aede757678f1662e276f813679651d3f4f7dfd9aa3da036bd3ff8fba48f")
                 .build()

             val req = chain.request()
                 .newBuilder()
                 .url(url)
                 .build()

               return@Interceptor chain.proceed(req)
           }

           val logging = HttpLoggingInterceptor()
           logging.level = HttpLoggingInterceptor.Level.BODY

           val okHttpClient = OkHttpClient.Builder()
               .addInterceptor(requestInterceptor)
               .addInterceptor(logging)
               .build()


           return Retrofit.Builder()
               .client(okHttpClient)
               .baseUrl("https://api.unsplash.com/")
               .addCallAdapterFactory(CoroutineCallAdapterFactory())
               .addConverterFactory(GsonConverterFactory.create())
               .build()
               .create(NetworkApiService::class.java)
       }
    }

    @GET("/search/photos/")
    fun searchPhotoAsync(
        @Query("page") page: Int,
        @Query("query") query: String
    ): Deferred<SearchResp>

    @GET("/photos")
    fun getDailyPhotoAsync(
        @Query("page") page: Int = 1,
        @Query("per_page") perPage: Int = 10,
        @Query("order_by") orderBy: String = "popular"
    ): Deferred<List<DailyResp>>

    @GET("/photos/{id}")
    fun getPhotoAsync(@Path("id") id: String): Deferred<PhotoItemResp>

    @GET("/collections")
    fun fetchCollectionsAsync(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Deferred<List<GalleryResp>>

    @GET("/collections/{id}/photos")
    fun getCollectionPhotosByIdAsync(
        @Path("id") id: Int,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Deferred<List<PhotoResp>>
}