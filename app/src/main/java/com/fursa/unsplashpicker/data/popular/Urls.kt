package com.fursa.unsplashpicker.data.popular



data class Urls(
    val full: String,
    val raw: String,
    val regular: String,
    val small: String,
    val thumb: String
)