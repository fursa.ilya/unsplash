package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class AncestryX(
    val type: TypeX
)