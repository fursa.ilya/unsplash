package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class UrlsXXX(
    val full: String,
    val raw: String,
    val regular: String,
    val small: String,
    val thumb: String
)