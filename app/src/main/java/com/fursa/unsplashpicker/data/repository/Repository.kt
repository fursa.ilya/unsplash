package com.fursa.unsplashpicker.data.repository

import com.fursa.unsplashpicker.data.collections.GalleryResp
import com.fursa.unsplashpicker.data.photo.PhotoItemResp
import com.fursa.unsplashpicker.data.photos.PhotoResp
import com.fursa.unsplashpicker.data.popular.DailyResp
import com.fursa.unsplashpicker.data.search.SearchResp

interface UnsplashRepository {
    suspend fun fetchCollectionsAsync(page: Int, perPage: Int): List<GalleryResp>
    suspend fun getCollectionByIdAsync(id: Int, page: Int, perPage: Int): List<PhotoResp>
    suspend fun searchPhotoAsync(query: String, page: Int): SearchResp
    suspend fun getPhotoAsync(id: String): PhotoItemResp
    suspend fun getDailyPhotoAsync(): List<DailyResp>
}