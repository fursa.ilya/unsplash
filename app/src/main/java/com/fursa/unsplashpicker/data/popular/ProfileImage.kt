package com.fursa.unsplashpicker.data.popular


import com.google.gson.annotations.SerializedName

data class ProfileImage(
    val large: String,
    val medium: String,
    val small: String
)