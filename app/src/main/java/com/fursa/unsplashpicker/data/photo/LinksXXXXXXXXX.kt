package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class LinksXXXXXXXXX(
    val followers: String,
    val following: String,
    val html: String,
    val likes: String,
    val photos: String,
    val portfolio: String,
    val self: String
)