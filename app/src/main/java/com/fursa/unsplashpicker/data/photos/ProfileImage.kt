package com.fursa.unsplashpicker.data.photos


import com.google.gson.annotations.SerializedName

data class ProfileImage(
    val large: String,
    val medium: String,
    val small: String
)