package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class ProfileImageXXXX(
    val large: String,
    val medium: String,
    val small: String
)