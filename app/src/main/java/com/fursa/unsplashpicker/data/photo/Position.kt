package com.fursa.unsplashpicker.data.photo


import com.google.gson.annotations.SerializedName

data class Position(
    val latitude: Any,
    val longitude: Any
)