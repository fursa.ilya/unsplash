package com.fursa.unsplashpicker.data.repository

import com.fursa.unsplashpicker.AppDelegate
import com.fursa.unsplashpicker.data.NetworkApiService
import com.fursa.unsplashpicker.data.collections.GalleryResp
import com.fursa.unsplashpicker.data.photo.PhotoItemResp
import com.fursa.unsplashpicker.data.photos.PhotoResp
import com.fursa.unsplashpicker.data.popular.DailyResp
import com.fursa.unsplashpicker.data.search.SearchResp
import javax.inject.Inject


class UnsplashRepositoryImpl @Inject constructor(private val apiService: NetworkApiService): UnsplashRepository {

    init {
        AppDelegate.appComponent.inject(this)
    }

    override suspend fun fetchCollectionsAsync(
        page: Int,
        perPage: Int
    ): List<GalleryResp> {
        return apiService.fetchCollectionsAsync(page, perPage).await()
    }

    override suspend fun getCollectionByIdAsync(
        id: Int,
        page: Int,
        perPage: Int
    ): List<PhotoResp> {
        return apiService.getCollectionPhotosByIdAsync(id, page, perPage).await()
    }

    override suspend fun searchPhotoAsync(query: String, page: Int): SearchResp {
        return apiService.searchPhotoAsync(page, query).await()
    }

    override suspend fun getPhotoAsync(id: String): PhotoItemResp {
        return apiService.getPhotoAsync(id).await()
    }

    override suspend fun getDailyPhotoAsync(): List<DailyResp> {
        return apiService.getDailyPhotoAsync().await()
    }
}