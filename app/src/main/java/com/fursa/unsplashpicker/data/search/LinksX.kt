package com.fursa.unsplashpicker.data.search


import com.google.gson.annotations.SerializedName

data class LinksX(
    val html: String,
    val likes: String,
    val photos: String,
    val self: String
)