package com.fursa.unsplashpicker.data.collections


import com.google.gson.annotations.SerializedName

data class Links(
    val download: String,
    val html: String,
    val self: String
)