package com.fursa.unsplashpicker.data.collections


import com.google.gson.annotations.SerializedName

data class LinksXX(
    val html: String,
    val photos: String,
    val related: String,
    val self: String
)