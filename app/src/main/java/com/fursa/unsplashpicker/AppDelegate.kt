package com.fursa.unsplashpicker

import android.app.Application
import com.fursa.unsplashpicker.di.AppComponent
import com.fursa.unsplashpicker.di.AppModule
import com.fursa.unsplashpicker.di.DaggerAppComponent
import com.fursa.unsplashpicker.di.NetworkModule

class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .networkModule(NetworkModule(this))
            .appModule(AppModule(this))
            .build()

        appComponent.inject(this)

    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}