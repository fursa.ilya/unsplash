package com.fursa.unsplashpicker.di

import com.fursa.unsplashpicker.AppDelegate
import com.fursa.unsplashpicker.data.repository.UnsplashRepository
import com.fursa.unsplashpicker.ui.UnsplashViewModel
import com.fursa.unsplashpicker.ui.collections.GalleryFragment
import com.fursa.unsplashpicker.ui.fullphoto.FullscreenPhotoFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        AppModule::class
    ]
)
interface AppComponent {
    fun inject(appDelegate: AppDelegate)
    fun inject(galleryFragment: GalleryFragment)
    fun inject(viewModel: UnsplashViewModel)
    fun inject(repository: UnsplashRepository)
    fun inject(fullscreenPhotoFragment: FullscreenPhotoFragment)
}