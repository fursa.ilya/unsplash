package com.fursa.unsplashpicker.di

import android.app.Application
import android.app.WallpaperManager
import com.fursa.unsplashpicker.data.NetworkApiService
import com.fursa.unsplashpicker.data.repository.UnsplashRepository
import com.fursa.unsplashpicker.data.repository.UnsplashRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideUnsplashRepository() = UnsplashRepositoryImpl(apiService = NetworkApiService.invoke())

    @Singleton
    @Provides
    fun provideContext() = application.applicationContext
}