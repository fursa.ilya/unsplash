package com.fursa.unsplashpicker.di

import android.app.Application
import com.fursa.unsplashpicker.data.NetworkApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule(private val application: Application) {
    private val context = application.applicationContext

    @Singleton
    @Provides
    fun provideApiService() = NetworkApiService()
}