package com.fursa.unsplashpicker.ui.collection

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl

class CurrentAdapter: RecyclerView.Adapter<CurrentAdapter.CurrentViewHolder>() {
    private val currentItems = mutableListOf<CurrentItem>()
    private lateinit var listener: CurrentItemClickListener

    fun setListener(listener: CurrentItemClickListener) {
        this.listener = listener
    }

    fun setItems(currentItems: MutableList<CurrentItem>) {
        this.currentItems.clear()
        this.currentItems.addAll(currentItems)
        notifyDataSetChanged()
    }

    inner class CurrentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val currentImageView: ImageView = itemView.findViewById(R.id.imageView)

        fun bind(currentItem: CurrentItem) {
            currentImageView.loadUrl(currentItem.photoUrl)
            itemView.setOnClickListener {
                listener.onCurrentItemSelected(currentItem.id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentViewHolder {
        val currentImageView = LayoutInflater.from(parent.context).inflate(R.layout.gallery_row, parent, false)
        return CurrentViewHolder(currentImageView)
    }

    override fun getItemCount() = currentItems.count()

    override fun onBindViewHolder(holder: CurrentViewHolder, position: Int) {
        val item = currentItems[position]
        holder.bind(item)
    }

    interface CurrentItemClickListener {
        fun onCurrentItemSelected(id: String)
    }
}