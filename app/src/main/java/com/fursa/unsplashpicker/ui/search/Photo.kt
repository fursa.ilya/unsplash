package com.fursa.unsplashpicker.ui.search

data class Photo(val photoUrl: String)