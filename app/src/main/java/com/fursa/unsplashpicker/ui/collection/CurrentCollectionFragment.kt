package com.fursa.unsplashpicker.ui.collection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.fursa.unsplashpicker.ui.MainActivity
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.mapToCollectionItems
import com.fursa.unsplashpicker.extensions.toast
import com.fursa.unsplashpicker.ui.UnsplashViewModel
import kotlinx.android.synthetic.main.current_collection_fragment.*

class CurrentCollectionFragment : Fragment() {
    private val currentAdapter = CurrentAdapter()
    private val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
    private lateinit var viewModel: UnsplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.current_collection_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UnsplashViewModel::class.java)
        currentAdapter.setListener(activity as MainActivity)

        with(imageRecycler) {
            adapter = currentAdapter
            layoutManager = staggeredGridLayoutManager
        }

        val id = arguments!!.getInt("id")
        viewModel.getCollection(id, 1, 30).observe(this, Observer {
            currentProgressBar.visibility = View.GONE
            imageRecycler.visibility = View.VISIBLE
            currentAdapter.setItems(it.mapToCollectionItems())
        })

        imageRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }
}
