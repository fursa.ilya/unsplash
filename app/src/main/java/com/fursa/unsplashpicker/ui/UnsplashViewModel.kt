package com.fursa.unsplashpicker.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.fursa.unsplashpicker.AppDelegate
import com.fursa.unsplashpicker.data.collections.GalleryResp
import com.fursa.unsplashpicker.data.photo.PhotoItemResp
import com.fursa.unsplashpicker.data.photos.PhotoResp
import com.fursa.unsplashpicker.data.popular.DailyResp
import com.fursa.unsplashpicker.data.repository.UnsplashRepository
import com.fursa.unsplashpicker.data.repository.UnsplashRepositoryImpl
import com.fursa.unsplashpicker.data.search.SearchResp
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Inject

class UnsplashViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var repository: UnsplashRepositoryImpl

    init {
        AppDelegate.appComponent.inject(this)
    }

    fun getCollections(page: Int, perPage: Int): MutableLiveData<List<GalleryResp>> {
        val collectionsLive = MutableLiveData<List<GalleryResp>>()
        viewModelScope.launch {
            val collections = repository.fetchCollectionsAsync(page, perPage)
            collectionsLive.value = collections
        }

        return collectionsLive
    }

    fun getCollection(id: Int, page: Int, perPage: Int): MutableLiveData<List<PhotoResp>> {
        val collectionLive = MutableLiveData<List<PhotoResp>>()
        viewModelScope.launch {
            val collection = repository.getCollectionByIdAsync(id, page, perPage)
            collectionLive.value = collection
        }

        return collectionLive
    }

    fun getPhoto(id: String): MutableLiveData<PhotoItemResp> {
        val photoLive = MutableLiveData<PhotoItemResp>()
        viewModelScope.launch {
            val photo = repository.getPhotoAsync(id)
            photoLive.value = photo
        }

        return photoLive
    }

    fun getDailyPhoto(): MutableLiveData<List<DailyResp>> {
        val dailyPhotoLive = MutableLiveData<List<DailyResp>>()
        viewModelScope.launch {
            val dailyPhoto = repository.getDailyPhotoAsync()
            dailyPhotoLive.value = dailyPhoto
        }

        return dailyPhotoLive
    }

    fun fetchPhotosByQuery(query: String, page: Int): MutableLiveData<SearchResp> {
        val searchLive = MutableLiveData<SearchResp>()
        viewModelScope.launch {
            val search = repository.searchPhotoAsync(query, page)
            searchLive.value = search
        }

        return searchLive
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}