package com.fursa.unsplashpicker.ui.daily

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl
import com.fursa.unsplashpicker.ui.UnsplashViewModel
import kotlinx.android.synthetic.main.daily_picture_fragment.*

class DailyPictureFragment : Fragment() {
    private lateinit var viewModel: UnsplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.daily_picture_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UnsplashViewModel::class.java)

        viewModel.getDailyPhoto().observe(this, Observer { popularPhotos ->
            val result = popularPhotos.maxBy { it.likes }
            civUserProfileImage.loadUrl(result!!.user.profileImage.large)
            txtUsername.text = result.user.name
            txtInstagramName.text = "@${result.user.instagramUsername}"
            txtLikes.text = result.likes.toString()
            imageDailyPic.loadUrl(result.urls.regular)
            txtDimens.text = "${resources.getString(R.string.width)} ${result.width} ${resources.getString(R.string.height)} ${result.height}"
            progressBarDaily.visibility = View.GONE
            contentLayout.visibility = View.VISIBLE

            imageDailyPic.setOnClickListener {
                val args = bundleOf("photo_url" to result.urls.full)
                findNavController().navigate(R.id.fullscreenPhotoFragment, args)
            }

        })


    }
}
