package com.fursa.unsplashpicker.ui.dialogs

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl
import com.fursa.unsplashpicker.ui.UnsplashViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.info_bottom_sheet_fragment.*
import java.net.URL

const val PHOTO_ID = "photoId"

class InfoBottomSheet : BottomSheetDialogFragment() {
    private lateinit var viewModel: UnsplashViewModel

    private lateinit var fullPictureUrl: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.info_bottom_sheet_fragment, container, false)
    }

    override fun getTheme(): Int {
        return R.style.BottomSheetDialogTheme
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), theme)
    }

    companion object {
        fun newInstance(id: String): InfoBottomSheet {
            val fragment = InfoBottomSheet()
            val args = Bundle().apply {
                putString(PHOTO_ID, id)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UnsplashViewModel::class.java)
        val photoId = arguments?.getString(PHOTO_ID)

        viewModel.getPhoto(photoId!!).observe(this, Observer {
            txtDimen.text = "${it.width}x${it.height}"
            txtInfo.text = it.description
            txtUsername.text = it.user.name
            txtInstagramName.text = "@${it.user.instagramUsername}"
            civUserAvatar.loadUrl(it.user.profileImage.large)
            progressBarInfo.visibility = View.GONE
            dialogLayout.visibility = View.VISIBLE

            if(it.urls.full.isNotEmpty()) {
                fullPictureUrl = it.urls.full
            }
         })

        btnOpen.setOnClickListener {
            if(fullPictureUrl.isNotEmpty()) {
                val args = bundleOf("photo_url" to fullPictureUrl)
                findNavController().navigate(R.id.fullscreenPhotoFragment, args)
            }
        }
    }
}