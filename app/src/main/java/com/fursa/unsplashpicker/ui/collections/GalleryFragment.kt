package com.fursa.unsplashpicker.ui.collections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.mapToGalleryItem
import com.fursa.unsplashpicker.ui.UnsplashViewModel
import com.fursa.unsplashpicker.ui.collections.adapter.CollectionsAdapter
import kotlinx.android.synthetic.main.gallery_fragment.*

class GalleryFragment : Fragment() {
    private val galleryAdapter = CollectionsAdapter()
    private val linearLayoutManager = LinearLayoutManager(context)

    private lateinit var viewModel: UnsplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gallery_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UnsplashViewModel::class.java)

        with(galleryRecycler) {
            adapter = galleryAdapter
            layoutManager = linearLayoutManager
        }

        loadGalleryItems(1)
    }

    private fun loadGalleryItems(page: Int) {
        viewModel.getCollections(page, 10).observe(this, Observer { collections ->
            galleryRecycler.visibility = View.VISIBLE
            progressBarGallery.visibility = View.GONE
            galleryAdapter.setItems(collections.mapToGalleryItem())
        })
    }


}
