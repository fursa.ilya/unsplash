package com.fursa.unsplashpicker.ui.fullphoto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl
import com.fursa.unsplashpicker.ui.MainActivity
import kotlinx.android.synthetic.main.fullscreen_photo_layout.*

class FullscreenPhotoFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()
        val currentDialog = dialog
        if(dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            currentDialog?.window?.setLayout(width, height)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fullscreen_photo_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setSupportActionBar(toolbarView)
        toolbarView.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbarView.setNavigationOnClickListener { findNavController().popBackStack() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val photoUrl  = arguments!!.getString("photo_url")
        imageViewFull.loadUrl("$photoUrl")

    }

}