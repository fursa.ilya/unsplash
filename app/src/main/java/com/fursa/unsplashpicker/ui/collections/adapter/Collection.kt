package com.fursa.unsplashpicker.ui.collections.adapter

data class Collection(
    val id: Int,
    val photo: String,
    val title: String,
    val itemsCount: Int
)