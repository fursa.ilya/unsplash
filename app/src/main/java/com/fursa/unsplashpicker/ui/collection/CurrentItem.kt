package com.fursa.unsplashpicker.ui.collection

data class CurrentItem(
    val id: String,
    val photoUrl: String)