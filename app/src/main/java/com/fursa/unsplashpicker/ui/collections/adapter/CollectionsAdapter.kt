package com.fursa.unsplashpicker.ui.collections.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl


class CollectionsAdapter : RecyclerView.Adapter<CollectionsAdapter.GalleryViewHolder>() {
    private var collections = mutableListOf<Collection>()

    fun setItems(collections: MutableList<Collection>) {
        this.collections.clear()
        this.collections.addAll(collections)
        notifyDataSetChanged()
    }

    inner class GalleryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val galleryImageView: ImageView = itemView.findViewById(R.id.collectionImageView)
        private val txtGalleryTitle: TextView = itemView.findViewById(R.id.txtCollectionName)
        private val txtItemsCount: TextView = itemView.findViewById(R.id.txtItemsCount)

        fun bind(item: Collection) {
            galleryImageView.loadUrl(item.photo)
            txtGalleryTitle.text = item.title
            txtItemsCount.text = "${item.itemsCount} ${itemView.resources.getString(R.string.total_photos)}"

            itemView.setOnClickListener { view ->
               val args = bundleOf("id" to item.id)
               view.findNavController().navigate(R.id.currentCollectionFragment, args)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val galleryView = LayoutInflater.from(parent.context).inflate(R.layout.collection_row, parent, false)
        return GalleryViewHolder(galleryView)
    }

    override fun getItemCount(): Int {
        return collections.count()
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {
        val collection = collections[position]
        holder.bind(collection)
    }
}