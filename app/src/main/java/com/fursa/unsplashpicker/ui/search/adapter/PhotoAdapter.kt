package com.fursa.unsplashpicker.ui.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.extensions.loadUrl
import com.fursa.unsplashpicker.ui.search.Photo

class PhotoAdapter: RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {

    private var photos = mutableListOf<Photo>()

    fun setPhotos(photos: List<Photo>) {
        this.photos.clear()
        this.photos.addAll(photos)
        notifyDataSetChanged()
    }

    inner class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val photoImageView: ImageView = itemView.findViewById(R.id.imageViewPhoto)

        fun bind(photo: Photo) {
            photoImageView.loadUrl(photo.photoUrl)

            photoImageView.setOnClickListener {
                val args = bundleOf("photo_url" to photo.photoUrl)
                itemView.findNavController().navigate(R.id.fullscreenPhotoFragment, args)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val photoView = LayoutInflater.from(parent.context).inflate(R.layout.photo_row, parent, false)
        return PhotoViewHolder(photoView)
    }

    override fun getItemCount() = photos.count()

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = photos[position]
        holder.bind(photo)
    }
}