@file:JvmName("ToastUtils")

package com.fursa.unsplashpicker.extensions

import android.content.Context
import android.widget.Toast

fun toast(message: String, ctx: Context) {
    Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show()
}