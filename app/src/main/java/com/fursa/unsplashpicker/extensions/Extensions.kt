package com.fursa.unsplashpicker.extensions

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.fursa.unsplashpicker.R
import com.fursa.unsplashpicker.data.collections.GalleryResp
import com.fursa.unsplashpicker.data.photos.PhotoResp
import com.fursa.unsplashpicker.data.search.Result
import com.fursa.unsplashpicker.ui.collection.CurrentItem
import com.fursa.unsplashpicker.ui.collections.adapter.Collection
import com.fursa.unsplashpicker.ui.search.Photo

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun ImageView.loadUrl(url: String) {
    Glide.with(this.context)
        .load(url)
        .placeholder(R.drawable.empty)
        .centerCrop()
        .into(this)
}


fun List<GalleryResp>.mapToGalleryItem(): MutableList<Collection> {
    val result = mutableListOf<Collection>()
    this.forEach {
        result.add(
            Collection(
                id = it.id,
                photo = it.coverPhoto.urls.raw,
                title = it.title,
                itemsCount = it.totalPhotos)
        )
    }

    return result
}

fun List<PhotoResp>.mapToCollectionItems(): MutableList<CurrentItem> {
    val result = mutableListOf<CurrentItem>()
    this.forEach {
        result.add(
            CurrentItem(
            id = it.id,
            photoUrl = it.urls.regular
        ))
    }

    return result
}

fun List<Result>.mapToPhotos(): MutableList<Photo>  {
    val result = mutableListOf<Photo>()
    this.forEach {
        result.add(Photo(it.urls.full))
    }

    return result
}